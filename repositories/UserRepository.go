package Repository

import (
	entity "backend-gateway-service/Entity"
	"backend-gateway-service/appConfiguration/logger"
	"backend-gateway-service/infrastructure"
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type IUserRepository interface {
	GetAll() ([]entity.User, error)
}

type UserRepositoryImpl struct {
	collection *mongo.Collection
}

func NewUserRepository(db *infrastructure.MongoDB) IUserRepository {
	collection := db.Client().Database(db.DatabaseName()).Collection("users")
	return &UserRepositoryImpl{collection: collection}
}

func (r *UserRepositoryImpl) GetAll() ([]entity.User, error) {
	cursor, err := r.collection.Find(context.Background(), bson.M{})
	l := logger.NewLogger()
	if err != nil {
		l.Error("error at fetching users", logger.Field{Key: "error", Value: err.Error()})
		return nil, err
	}
	defer cursor.Close(context.Background())

	var users []entity.User
	if err := cursor.All(context.Background(), &users); err != nil {
		l.Error("error at fetching users", logger.Field{Key: "error", Value: err.Error()})
		return nil, err
	}

	return users, nil
}
