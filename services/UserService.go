package services

import (
	entity "backend-gateway-service/Entity"
	config "backend-gateway-service/appConfiguration/config"
	logger "backend-gateway-service/appConfiguration/logger"
	infrastructure "backend-gateway-service/infrastructure"
	repository "backend-gateway-service/repositories"

	"github.com/google/wire"
)

type IUserService interface {
	GetAllUsers() ([]entity.User, error)
}

type UserServiceImpl struct {
	UserRepository repository.IUserRepository
}

func NewUserServiceImpl(userRepository repository.IUserRepository) IUserService {
	return &UserServiceImpl{UserRepository: userRepository}
}

func (s *UserServiceImpl) GetAllUsers() ([]entity.User, error) {

	users, err := s.UserRepository.GetAll()
	l := logger.NewLogger()
	if err != nil {
		l.Error("fail at service", logger.Field{Key: "error", Value: err})
		return nil, err
	}
	
	return users, nil
}


var UserServiceSet = wire.NewSet(NewUserServiceImpl, repository.NewUserRepository, infrastructure.NewMongoDB, config.LoadConfig)