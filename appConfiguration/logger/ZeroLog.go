package logger

import (
	"fmt"
	"io"
	"os"
	"runtime"
	"time"

	"github.com/google/wire"
	"github.com/rs/zerolog"
	"gopkg.in/natefinch/lumberjack.v2"
)

// Logger represents a ZeroLog logger instance.
type ZeroLogger struct {
	logger zerolog.Logger
}

// NewLogger creates a new ZeroLog logger instance.
func NewLogger() *ZeroLogger {
	// return &ZeroLogger{logger: zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout}).With().Timestamp().Logger()}
	logFileName := "logs/" + time.Now().Format("2006-01-02_15-04-05") + ".log"

    // Configure log rotation with lumberjack
    logFile := &lumberjack.Logger{
        Filename:   logFileName,
        MaxSize:    10, // Max megabytes before log rotation
        MaxBackups: 5,  // Max number of old log files to keep
        MaxAge:     30, // Max days to retain old log files
        Compress:   true,
    }

    // // Create a multiwriter to write logs to both console and file
    multiWriter := io.MultiWriter(zerolog.ConsoleWriter{Out: os.Stdout}, logFile)

    // // Create a zerolog logger with the multiwriter
    logger := zerolog.New(multiWriter).With().Timestamp().Logger()

    // // Set log level to debug
    zerolog.SetGlobalLevel(zerolog.DebugLevel)

    return &ZeroLogger{logger: logger}
}

// Debug logs a debug message.
func (l *ZeroLogger) Debug(msg string, fields ...Field) {
	l.logger.Debug().Fields(fieldsToMap(fields...)).Msg(msg)
}

// Info logs an info message.
func (l *ZeroLogger) Info(msg string, fields ...Field) {
	l.logger.Info().Fields(fieldsToMap(fields...)).Msg(msg)
}

// Warn logs a warning message.
func (l *ZeroLogger) Warn(msg string, fields ...Field) {
	l.logger.Warn().Fields(fieldsToMap(fields...)).Msg(msg)
}

// Error logs an error message with stack trace.
func (l *ZeroLogger) Error(msg string, fields ...Field) {
	pc := make([]uintptr, 15)
	runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc)
	stack := make([]string, 0, 15)
	for frame, more := frames.Next(); more; frame, more = frames.Next() {
		stack = append(stack, fmt.Sprintf("%s:%d %s", frame.File, frame.Line, frame.Function))
	}
	fields = append(fields, Field{Key: "stacktrace", Value: stack})
	l.logger.Error().Fields(fieldsToMap(fields...)).Msg(msg)
}

// fieldsToMap converts a slice of Fields into a map.
func fieldsToMap(fields ...Field) map[string]interface{} {
	result := make(map[string]interface{}, len(fields))
	for _, field := range fields {
		result[field.Key] = field.Value
	}
	return result
}

var LoggerSet = wire.NewSet(NewLogger, wire.Bind(new(ILogger),new(*ZeroLogger)))