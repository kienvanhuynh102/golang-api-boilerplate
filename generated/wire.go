// go:build wireinject
// +build wireinject

package container

import (
	"backend-gateway-service/appConfiguration/logger"
	"backend-gateway-service/services"

	"github.com/google/wire"
)

func NewLogger() logger.ILogger{
	wire.Build(logger.LoggerSet)
	return &logger.ZeroLogger{}
}

func InitializeUserService() (services.IUserService, error) {
	wire.Build(services.UserServiceSet)
	return &services.UserServiceImpl{}, nil
}
