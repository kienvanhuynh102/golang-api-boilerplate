package handler

import (
	"backend-gateway-service/appConfiguration/logger"
	container "backend-gateway-service/generated"
	"backend-gateway-service/pb"
	"context"

)

func (s *Server) GetAllUsers(ctx context.Context, req *pb.GetAllUsersRequest) (*pb.GetAllUsersResponse, error) {
	
	userService, errService := container.InitializeUserService()
	if errService != nil {
		s.logger.Error("Failed to initialize user service", logger.Field{Key: "error", Value: errService})
	}

	res, err := userService.GetAllUsers()
	if err != nil {
		s.logger.Error("Failed to get user service", logger.Field{Key: "error", Value: err})
	}
	
	users := make([]*pb.User, 0, len(res))
	for i, u := range res {
		users[i] = &pb.User{Email: &u.Email, DisplayName: &u.Displayname}
	}
	response := &pb.GetAllUsersResponse{Users: users}
	return response, nil
}
