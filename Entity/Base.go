package entity

import (
	"time"

	"labix.org/v2/mgo/bson"
)

type BaseEntity struct {
	Id        bson.ObjectId `bson:"_id" json:"_id,omitempty"`
	CreatedAt time.Time
	UpdatedAt time.Time
}
