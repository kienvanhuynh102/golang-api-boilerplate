package entity

type User struct {
	BaseEntity
	Email       string
	Displayname string
}
