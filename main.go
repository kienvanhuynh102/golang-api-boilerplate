package main

import (
	
	"backend-gateway-service/handler"
	"sync"
)


func main() {
	var wg sync.WaitGroup
	s,_ := handler.NewServer()
	s.Start(&wg)
	wg.Wait()
}

